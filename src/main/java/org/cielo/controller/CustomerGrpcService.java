package org.cielo.controller;

import io.quarkus.grpc.GrpcService;

import io.smallrye.mutiny.Uni;
import org.cielo.service.ServiceImpl.CustomerServiceImpl;
import org.cielo.stubs.customer.CustomerGrpc;
import org.cielo.stubs.customer.CustomerSaveRequest;
import org.cielo.stubs.customer.CustomerSaveResponse;
import org.cielo.translator.CustomerTranslator;

import javax.inject.Inject;
import java.util.logging.Logger;

@GrpcService
public class CustomerGrpcService implements CustomerGrpc {
    private static final Logger LOGGER = Logger.getLogger("CustomerGrpcService.class");
    @Inject
    private CustomerServiceImpl customerService;

    private CustomerTranslator translator;

    @Override
    public Uni<CustomerSaveResponse> save(CustomerSaveRequest request) {
        LOGGER.info("Calling save on server");
        return CustomerTranslator.translateEntityToResponse(
                customerService.save(CustomerTranslator.translateRequestToEntity(request))
        );
    }
}
