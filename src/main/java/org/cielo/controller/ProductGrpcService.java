package org.cielo.controller;

import io.quarkus.grpc.GrpcService;
import io.smallrye.mutiny.Uni;
import org.cielo.stubs.customer.CustomerGrpc;
import org.cielo.stubs.customer.CustomerSaveRequest;
import org.cielo.stubs.customer.CustomerSaveResponse;
import org.cielo.stubs.product.ProductGrpc;
import org.cielo.stubs.product.ProductSaveRequest;
import org.cielo.stubs.product.ProductSaveResponse;


@GrpcService
public class ProductGrpcService implements ProductGrpc {

    @Override
    public Uni<ProductSaveResponse> save(ProductSaveRequest request) {
        return null;
    }
}
