package org.cielo.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.cielo.domain.Customer;

import javax.enterprise.context.ApplicationScoped;
import java.util.Objects;


@ApplicationScoped
public class CustomerRepository implements PanacheRepository<Customer> {

    public Customer save(Customer customer) {
        if (Objects.nonNull(customer.getId())) {
            throw new IllegalStateException("Id should be null!");
        }
        return save(customer);
    }

}
