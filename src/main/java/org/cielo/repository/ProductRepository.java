package org.cielo.repository;

import org.cielo.domain.Customer;
import org.cielo.domain.Product;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Objects;

@Transactional
@ApplicationScoped
public class ProductRepository {

    @PersistenceContext
    EntityManager em;

    public Product save(Product product) {
        if (Objects.nonNull(product.getId())) {
            throw new IllegalStateException("Id should be null!");
        }
        em.persist(product);
        return product;
    }
}
