package org.cielo.service;

import org.cielo.domain.Customer;

public interface CustomerService {

    Customer save(Customer customer);

}
