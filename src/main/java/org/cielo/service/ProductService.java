package org.cielo.service;

import org.cielo.domain.Product;

public interface ProductService {

    Product save(Product product);

}
