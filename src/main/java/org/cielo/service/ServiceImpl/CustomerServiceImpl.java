package org.cielo.service.ServiceImpl;

import org.cielo.domain.Customer;
import org.cielo.repository.CustomerRepository;
import org.cielo.service.CustomerService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.logging.Logger;

@ApplicationScoped
public class CustomerServiceImpl implements CustomerService {

    private static final Logger LOGGER = Logger.getLogger("CustomerService.class");

    @Inject
    CustomerRepository repository;

    @Override
    public Customer save(Customer customer) {
        LOGGER.info("Saving customer");
        return repository.save(customer);
    }
}
