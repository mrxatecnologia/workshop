package org.cielo.stubs.customer;

import io.quarkus.grpc.MutinyService;

@io.quarkus.grpc.common.Generated(value = "by Mutiny Grpc generator", comments = "Source: customer.proto")
public interface CustomerGrpc extends MutinyService {

    io.smallrye.mutiny.Uni<CustomerSaveResponse> save(CustomerSaveRequest request);
}

