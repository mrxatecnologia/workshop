package org.cielo.stubs.customer;

import io.grpc.BindableService;
import io.quarkus.grpc.GrpcService;
import io.quarkus.grpc.MutinyBean;

@io.quarkus.grpc.common.Generated(value = "by Mutiny Grpc generator", comments = "Source: customer.proto")
public class CustomerGrpcBean extends MutinyCustomerGrpcGrpc.CustomerGrpcImplBase implements BindableService, MutinyBean {

    private final CustomerGrpc delegate;

    CustomerGrpcBean(@GrpcService CustomerGrpc delegate) {
        this.delegate = delegate;
    }

    @Override
    public io.smallrye.mutiny.Uni<CustomerSaveResponse> save(CustomerSaveRequest request) {
        try {
            return delegate.save(request);
        } catch (UnsupportedOperationException e) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }
    }
}

