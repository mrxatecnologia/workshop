package org.cielo.stubs.customer;

import io.quarkus.grpc.MutinyClient;

import java.util.function.BiFunction;

@io.quarkus.grpc.common.Generated(value = "by Mutiny Grpc generator", comments = "Source: customer.proto")
public class CustomerGrpcClient implements CustomerGrpc, MutinyClient<MutinyCustomerGrpcGrpc.MutinyCustomerGrpcStub> {

    private final MutinyCustomerGrpcGrpc.MutinyCustomerGrpcStub stub;

    public CustomerGrpcClient(String name, io.grpc.Channel channel, BiFunction<String, MutinyCustomerGrpcGrpc.MutinyCustomerGrpcStub, MutinyCustomerGrpcGrpc.MutinyCustomerGrpcStub> stubConfigurator) {
        this.stub = stubConfigurator.apply(name, MutinyCustomerGrpcGrpc.newMutinyStub(channel));
    }

    private CustomerGrpcClient(MutinyCustomerGrpcGrpc.MutinyCustomerGrpcStub stub) {
        this.stub = stub;
    }

    public CustomerGrpcClient newInstanceWithStub(MutinyCustomerGrpcGrpc.MutinyCustomerGrpcStub stub) {
        return new CustomerGrpcClient(stub);
    }

    @Override
    public MutinyCustomerGrpcGrpc.MutinyCustomerGrpcStub getStub() {
        return stub;
    }

    @Override
    public io.smallrye.mutiny.Uni<CustomerSaveResponse> save(CustomerSaveRequest request) {
        return stub.save(request);
    }
}

