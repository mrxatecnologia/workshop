package org.cielo.stubs.customer;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@io.quarkus.grpc.common.Generated(value = "by gRPC proto compiler (version 1.49.0)", comments = "Source: customer.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class CustomerGrpcGrpc {

    private CustomerGrpcGrpc() {
    }

    public static final String SERVICE_NAME = "customer.CustomerGrpc";

    // Static method descriptors that strictly reflect the proto.
    private static volatile io.grpc.MethodDescriptor<CustomerSaveRequest, CustomerSaveResponse> getSaveMethod;

    @io.grpc.stub.annotations.RpcMethod(fullMethodName = SERVICE_NAME + '/' + "save", requestType = CustomerSaveRequest.class, responseType = CustomerSaveResponse.class, methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
    public static io.grpc.MethodDescriptor<CustomerSaveRequest, CustomerSaveResponse> getSaveMethod() {
        io.grpc.MethodDescriptor<CustomerSaveRequest, CustomerSaveResponse> getSaveMethod;
        if ((getSaveMethod = CustomerGrpcGrpc.getSaveMethod) == null) {
            synchronized (CustomerGrpcGrpc.class) {
                if ((getSaveMethod = CustomerGrpcGrpc.getSaveMethod) == null) {
                    CustomerGrpcGrpc.getSaveMethod = getSaveMethod = io.grpc.MethodDescriptor.<CustomerSaveRequest, CustomerSaveResponse>newBuilder().setType(io.grpc.MethodDescriptor.MethodType.UNARY).setFullMethodName(generateFullMethodName(SERVICE_NAME, "save")).setSampledToLocalTracing(true).setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(CustomerSaveRequest.getDefaultInstance())).setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(CustomerSaveResponse.getDefaultInstance())).setSchemaDescriptor(new CustomerGrpcMethodDescriptorSupplier("save")).build();
                }
            }
        }
        return getSaveMethod;
    }

    /**
     * Creates a new async stub that supports all call types for the service
     */
    public static CustomerGrpcStub newStub(io.grpc.Channel channel) {
        io.grpc.stub.AbstractStub.StubFactory<CustomerGrpcStub> factory = new io.grpc.stub.AbstractStub.StubFactory<CustomerGrpcStub>() {

            @Override
            public CustomerGrpcStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
                return new CustomerGrpcStub(channel, callOptions);
            }
        };
        return CustomerGrpcStub.newStub(factory, channel);
    }

    /**
     * Creates a new blocking-style stub that supports unary and streaming output calls on the service
     */
    public static CustomerGrpcBlockingStub newBlockingStub(io.grpc.Channel channel) {
        io.grpc.stub.AbstractStub.StubFactory<CustomerGrpcBlockingStub> factory = new io.grpc.stub.AbstractStub.StubFactory<CustomerGrpcBlockingStub>() {

            @Override
            public CustomerGrpcBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
                return new CustomerGrpcBlockingStub(channel, callOptions);
            }
        };
        return CustomerGrpcBlockingStub.newStub(factory, channel);
    }

    /**
     * Creates a new ListenableFuture-style stub that supports unary calls on the service
     */
    public static CustomerGrpcFutureStub newFutureStub(io.grpc.Channel channel) {
        io.grpc.stub.AbstractStub.StubFactory<CustomerGrpcFutureStub> factory = new io.grpc.stub.AbstractStub.StubFactory<CustomerGrpcFutureStub>() {

            @Override
            public CustomerGrpcFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
                return new CustomerGrpcFutureStub(channel, callOptions);
            }
        };
        return CustomerGrpcFutureStub.newStub(factory, channel);
    }

    /**
     */
    public static abstract class CustomerGrpcImplBase implements io.grpc.BindableService {

        /**
         */
        public void save(CustomerSaveRequest request, io.grpc.stub.StreamObserver<CustomerSaveResponse> responseObserver) {
            io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSaveMethod(), responseObserver);
        }

        @Override
        public io.grpc.ServerServiceDefinition bindService() {
            return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor()).addMethod(getSaveMethod(), io.grpc.stub.ServerCalls.asyncUnaryCall(new MethodHandlers<CustomerSaveRequest, CustomerSaveResponse>(this, METHODID_SAVE))).build();
        }
    }

    /**
     */
    public static class CustomerGrpcStub extends io.grpc.stub.AbstractAsyncStub<CustomerGrpcStub> {

        private CustomerGrpcStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override
        protected CustomerGrpcStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new CustomerGrpcStub(channel, callOptions);
        }

        /**
         */
        public void save(CustomerSaveRequest request, io.grpc.stub.StreamObserver<CustomerSaveResponse> responseObserver) {
            io.grpc.stub.ClientCalls.asyncUnaryCall(getChannel().newCall(getSaveMethod(), getCallOptions()), request, responseObserver);
        }
    }

    /**
     */
    public static class CustomerGrpcBlockingStub extends io.grpc.stub.AbstractBlockingStub<CustomerGrpcBlockingStub> {

        private CustomerGrpcBlockingStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override
        protected CustomerGrpcBlockingStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new CustomerGrpcBlockingStub(channel, callOptions);
        }

        /**
         */
        public CustomerSaveResponse save(CustomerSaveRequest request) {
            return io.grpc.stub.ClientCalls.blockingUnaryCall(getChannel(), getSaveMethod(), getCallOptions(), request);
        }
    }

    /**
     */
    public static class CustomerGrpcFutureStub extends io.grpc.stub.AbstractFutureStub<CustomerGrpcFutureStub> {

        private CustomerGrpcFutureStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override
        protected CustomerGrpcFutureStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new CustomerGrpcFutureStub(channel, callOptions);
        }

        /**
         */
        public com.google.common.util.concurrent.ListenableFuture<CustomerSaveResponse> save(CustomerSaveRequest request) {
            return io.grpc.stub.ClientCalls.futureUnaryCall(getChannel().newCall(getSaveMethod(), getCallOptions()), request);
        }
    }

    private static final int METHODID_SAVE = 0;

    private static final class MethodHandlers<Req, Resp> implements io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>, io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>, io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>, io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {

        private final CustomerGrpcImplBase serviceImpl;

        private final int methodId;

        MethodHandlers(CustomerGrpcImplBase serviceImpl, int methodId) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
        }

        @Override
        @SuppressWarnings("unchecked")
        public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch(methodId) {
                case METHODID_SAVE:
                    serviceImpl.save((CustomerSaveRequest) request, (io.grpc.stub.StreamObserver<CustomerSaveResponse>) responseObserver);
                    break;
                default:
                    throw new AssertionError();
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public io.grpc.stub.StreamObserver<Req> invoke(io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch(methodId) {
                default:
                    throw new AssertionError();
            }
        }
    }

    private static abstract class CustomerGrpcBaseDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {

        CustomerGrpcBaseDescriptorSupplier() {
        }

        @Override
        public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
            return CustomerGrpcProto.getDescriptor();
        }

        @Override
        public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
            return getFileDescriptor().findServiceByName("CustomerGrpc");
        }
    }

    private static final class CustomerGrpcFileDescriptorSupplier extends CustomerGrpcBaseDescriptorSupplier {

        CustomerGrpcFileDescriptorSupplier() {
        }
    }

    private static final class CustomerGrpcMethodDescriptorSupplier extends CustomerGrpcBaseDescriptorSupplier implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {

        private final String methodName;

        CustomerGrpcMethodDescriptorSupplier(String methodName) {
            this.methodName = methodName;
        }

        @Override
        public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
            return getServiceDescriptor().findMethodByName(methodName);
        }
    }

    private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

    public static io.grpc.ServiceDescriptor getServiceDescriptor() {
        io.grpc.ServiceDescriptor result = serviceDescriptor;
        if (result == null) {
            synchronized (CustomerGrpcGrpc.class) {
                result = serviceDescriptor;
                if (result == null) {
                    serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME).setSchemaDescriptor(new CustomerGrpcFileDescriptorSupplier()).addMethod(getSaveMethod()).build();
                }
            }
        }
        return result;
    }
}

