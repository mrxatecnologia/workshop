// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: customer.proto
package org.cielo.stubs.customer;

public interface CustomerSaveRequestOrBuilder extends // @@protoc_insertion_point(interface_extends:customer.CustomerSaveRequest)
com.google.protobuf.MessageOrBuilder {

    /**
     * <code>string name = 1;</code>
     * @return The name.
     */
    String getName();

    /**
     * <code>string name = 1;</code>
     * @return The bytes for name.
     */
    com.google.protobuf.ByteString getNameBytes();

    /**
     * <code>int32 age = 2;</code>
     * @return The age.
     */
    int getAge();

    /**
     * <code>string streetAddress = 3;</code>
     * @return The streetAddress.
     */
    String getStreetAddress();

    /**
     * <code>string streetAddress = 3;</code>
     * @return The bytes for streetAddress.
     */
    com.google.protobuf.ByteString getStreetAddressBytes();
}

