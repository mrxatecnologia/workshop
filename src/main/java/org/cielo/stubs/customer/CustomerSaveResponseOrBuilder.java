// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: customer.proto
package org.cielo.stubs.customer;

public interface CustomerSaveResponseOrBuilder extends // @@protoc_insertion_point(interface_extends:customer.CustomerSaveResponse)
com.google.protobuf.MessageOrBuilder {

    /**
     * <code>int32 responseCode = 1;</code>
     * @return The responseCode.
     */
    int getResponseCode();

    /**
     * <code>string responseMessage = 2;</code>
     * @return The responseMessage.
     */
    String getResponseMessage();

    /**
     * <code>string responseMessage = 2;</code>
     * @return The bytes for responseMessage.
     */
    com.google.protobuf.ByteString getResponseMessageBytes();
}

