package org.cielo.stubs.customer;

import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static org.cielo.stubs.customer.CustomerGrpcGrpc.getServiceDescriptor;

@io.quarkus.grpc.common.Generated(value = "by Mutiny Grpc generator", comments = "Source: customer.proto")
public final class MutinyCustomerGrpcGrpc implements io.quarkus.grpc.MutinyGrpc {

    private MutinyCustomerGrpcGrpc() {
    }

    public static MutinyCustomerGrpcStub newMutinyStub(io.grpc.Channel channel) {
        return new MutinyCustomerGrpcStub(channel);
    }

    public static class MutinyCustomerGrpcStub extends io.grpc.stub.AbstractStub<MutinyCustomerGrpcStub> implements io.quarkus.grpc.MutinyStub {

        private CustomerGrpcGrpc.CustomerGrpcStub delegateStub;

        private MutinyCustomerGrpcStub(io.grpc.Channel channel) {
            super(channel);
            delegateStub = CustomerGrpcGrpc.newStub(channel);
        }

        private MutinyCustomerGrpcStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
            delegateStub = CustomerGrpcGrpc.newStub(channel).build(channel, callOptions);
        }

        @Override
        protected MutinyCustomerGrpcStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new MutinyCustomerGrpcStub(channel, callOptions);
        }

        public io.smallrye.mutiny.Uni<CustomerSaveResponse> save(CustomerSaveRequest request) {
            return io.quarkus.grpc.stubs.ClientCalls.oneToOne(request, delegateStub::save);
        }
    }

    public static abstract class CustomerGrpcImplBase implements io.grpc.BindableService {

        private String compression;

        /**
         * Set whether the server will try to use a compressed response.
         *
         * @param compression the compression, e.g {@code gzip}
         */
        public CustomerGrpcImplBase withCompression(String compression) {
            this.compression = compression;
            return this;
        }

        public io.smallrye.mutiny.Uni<CustomerSaveResponse> save(CustomerSaveRequest request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        @Override
        public io.grpc.ServerServiceDefinition bindService() {
            return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor()).addMethod(CustomerGrpcGrpc.getSaveMethod(), asyncUnaryCall(new MethodHandlers<CustomerSaveRequest, CustomerSaveResponse>(this, METHODID_SAVE, compression))).build();
        }
    }

    private static final int METHODID_SAVE = 0;

    private static final class MethodHandlers<Req, Resp> implements io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>, io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>, io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>, io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {

        private final CustomerGrpcImplBase serviceImpl;

        private final int methodId;

        private final String compression;

        MethodHandlers(CustomerGrpcImplBase serviceImpl, int methodId, String compression) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
            this.compression = compression;
        }

        @Override
        @SuppressWarnings("unchecked")
        public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch(methodId) {
                case METHODID_SAVE:
                    io.quarkus.grpc.stubs.ServerCalls.oneToOne((CustomerSaveRequest) request, (io.grpc.stub.StreamObserver<CustomerSaveResponse>) responseObserver, compression, serviceImpl::save);
                    break;
                default:
                    throw new AssertionError();
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public io.grpc.stub.StreamObserver<Req> invoke(io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch(methodId) {
                default:
                    throw new AssertionError();
            }
        }
    }
}

