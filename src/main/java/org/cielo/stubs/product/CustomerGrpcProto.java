// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: product.proto
package org.cielo.stubs.product;

public final class CustomerGrpcProto {

    private CustomerGrpcProto() {
    }

    public static void registerAllExtensions(com.google.protobuf.ExtensionRegistryLite registry) {
    }

    public static void registerAllExtensions(com.google.protobuf.ExtensionRegistry registry) {
        registerAllExtensions((com.google.protobuf.ExtensionRegistryLite) registry);
    }

    static final com.google.protobuf.Descriptors.Descriptor internal_static_product_ProductSaveRequest_descriptor;

    static final com.google.protobuf.GeneratedMessageV3.FieldAccessorTable internal_static_product_ProductSaveRequest_fieldAccessorTable;

    static final com.google.protobuf.Descriptors.Descriptor internal_static_product_ProductSaveResponse_descriptor;

    static final com.google.protobuf.GeneratedMessageV3.FieldAccessorTable internal_static_product_ProductSaveResponse_fieldAccessorTable;

    public static com.google.protobuf.Descriptors.FileDescriptor getDescriptor() {
        return descriptor;
    }

    private static com.google.protobuf.Descriptors.FileDescriptor descriptor;

    static {
        String[] descriptorData = { "\n\rproduct.proto\022\007product\"8\n\022ProductSaveR" + "equest\022\023\n\013description\030\001 \001(\t\022\r\n\005price\030\002 \001" + "(\003\"D\n\023ProductSaveResponse\022\024\n\014responseCod" + "e\030\001 \001(\005\022\027\n\017responseMessage\030\002 \001(\t2R\n\013Prod" + "uctGrpc\022C\n\004save\022\033.product.ProductSaveReq" + "uest\032\034.product.ProductSaveResponse\"\000B.\n\027" + "org.cielo.stubs.productB\021CustomerGrpcPro" + "toP\001b\006proto3" };
        descriptor = com.google.protobuf.Descriptors.FileDescriptor.internalBuildGeneratedFileFrom(descriptorData, new com.google.protobuf.Descriptors.FileDescriptor[] {});
        internal_static_product_ProductSaveRequest_descriptor = getDescriptor().getMessageTypes().get(0);
        internal_static_product_ProductSaveRequest_fieldAccessorTable = new com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(internal_static_product_ProductSaveRequest_descriptor, new String[] { "Description", "Price" });
        internal_static_product_ProductSaveResponse_descriptor = getDescriptor().getMessageTypes().get(1);
        internal_static_product_ProductSaveResponse_fieldAccessorTable = new com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(internal_static_product_ProductSaveResponse_descriptor, new String[] { "ResponseCode", "ResponseMessage" });
    }
    // @@protoc_insertion_point(outer_class_scope)
}

