package org.cielo.stubs.product;

import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static org.cielo.stubs.product.ProductGrpcGrpc.getServiceDescriptor;

@io.quarkus.grpc.common.Generated(value = "by Mutiny Grpc generator", comments = "Source: product.proto")
public final class MutinyProductGrpcGrpc implements io.quarkus.grpc.MutinyGrpc {

    private MutinyProductGrpcGrpc() {
    }

    public static MutinyProductGrpcStub newMutinyStub(io.grpc.Channel channel) {
        return new MutinyProductGrpcStub(channel);
    }

    public static class MutinyProductGrpcStub extends io.grpc.stub.AbstractStub<MutinyProductGrpcStub> implements io.quarkus.grpc.MutinyStub {

        private ProductGrpcGrpc.ProductGrpcStub delegateStub;

        private MutinyProductGrpcStub(io.grpc.Channel channel) {
            super(channel);
            delegateStub = ProductGrpcGrpc.newStub(channel);
        }

        private MutinyProductGrpcStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
            delegateStub = ProductGrpcGrpc.newStub(channel).build(channel, callOptions);
        }

        @Override
        protected MutinyProductGrpcStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new MutinyProductGrpcStub(channel, callOptions);
        }

        public io.smallrye.mutiny.Uni<ProductSaveResponse> save(ProductSaveRequest request) {
            return io.quarkus.grpc.stubs.ClientCalls.oneToOne(request, delegateStub::save);
        }
    }

    public static abstract class ProductGrpcImplBase implements io.grpc.BindableService {

        private String compression;

        /**
         * Set whether the server will try to use a compressed response.
         *
         * @param compression the compression, e.g {@code gzip}
         */
        public ProductGrpcImplBase withCompression(String compression) {
            this.compression = compression;
            return this;
        }

        public io.smallrye.mutiny.Uni<ProductSaveResponse> save(ProductSaveRequest request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        @Override
        public io.grpc.ServerServiceDefinition bindService() {
            return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor()).addMethod(ProductGrpcGrpc.getSaveMethod(), asyncUnaryCall(new MethodHandlers<ProductSaveRequest, ProductSaveResponse>(this, METHODID_SAVE, compression))).build();
        }
    }

    private static final int METHODID_SAVE = 0;

    private static final class MethodHandlers<Req, Resp> implements io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>, io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>, io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>, io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {

        private final ProductGrpcImplBase serviceImpl;

        private final int methodId;

        private final String compression;

        MethodHandlers(ProductGrpcImplBase serviceImpl, int methodId, String compression) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
            this.compression = compression;
        }

        @Override
        @SuppressWarnings("unchecked")
        public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch(methodId) {
                case METHODID_SAVE:
                    io.quarkus.grpc.stubs.ServerCalls.oneToOne((ProductSaveRequest) request, (io.grpc.stub.StreamObserver<ProductSaveResponse>) responseObserver, compression, serviceImpl::save);
                    break;
                default:
                    throw new AssertionError();
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public io.grpc.stub.StreamObserver<Req> invoke(io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch(methodId) {
                default:
                    throw new AssertionError();
            }
        }
    }
}

