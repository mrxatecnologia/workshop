package org.cielo.stubs.product;

import io.quarkus.grpc.MutinyService;

@io.quarkus.grpc.common.Generated(value = "by Mutiny Grpc generator", comments = "Source: product.proto")
public interface ProductGrpc extends MutinyService {

    io.smallrye.mutiny.Uni<ProductSaveResponse> save(ProductSaveRequest request);
}

