package org.cielo.stubs.product;

import io.grpc.BindableService;
import io.quarkus.grpc.GrpcService;
import io.quarkus.grpc.MutinyBean;

@io.quarkus.grpc.common.Generated(value = "by Mutiny Grpc generator", comments = "Source: product.proto")
public class ProductGrpcBean extends MutinyProductGrpcGrpc.ProductGrpcImplBase implements BindableService, MutinyBean {

    private final ProductGrpc delegate;

    ProductGrpcBean(@GrpcService ProductGrpc delegate) {
        this.delegate = delegate;
    }

    @Override
    public io.smallrye.mutiny.Uni<ProductSaveResponse> save(ProductSaveRequest request) {
        try {
            return delegate.save(request);
        } catch (UnsupportedOperationException e) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }
    }
}

