package org.cielo.stubs.product;

import io.quarkus.grpc.MutinyClient;

import java.util.function.BiFunction;

@io.quarkus.grpc.common.Generated(value = "by Mutiny Grpc generator", comments = "Source: product.proto")
public class ProductGrpcClient implements ProductGrpc, MutinyClient<MutinyProductGrpcGrpc.MutinyProductGrpcStub> {

    private final MutinyProductGrpcGrpc.MutinyProductGrpcStub stub;

    public ProductGrpcClient(String name, io.grpc.Channel channel, BiFunction<String, MutinyProductGrpcGrpc.MutinyProductGrpcStub, MutinyProductGrpcGrpc.MutinyProductGrpcStub> stubConfigurator) {
        this.stub = stubConfigurator.apply(name, MutinyProductGrpcGrpc.newMutinyStub(channel));
    }

    private ProductGrpcClient(MutinyProductGrpcGrpc.MutinyProductGrpcStub stub) {
        this.stub = stub;
    }

    public ProductGrpcClient newInstanceWithStub(MutinyProductGrpcGrpc.MutinyProductGrpcStub stub) {
        return new ProductGrpcClient(stub);
    }

    @Override
    public MutinyProductGrpcGrpc.MutinyProductGrpcStub getStub() {
        return stub;
    }

    @Override
    public io.smallrye.mutiny.Uni<ProductSaveResponse> save(ProductSaveRequest request) {
        return stub.save(request);
    }
}

