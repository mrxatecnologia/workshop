package org.cielo.stubs.product;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@io.quarkus.grpc.common.Generated(value = "by gRPC proto compiler (version 1.49.0)", comments = "Source: product.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class ProductGrpcGrpc {

    private ProductGrpcGrpc() {
    }

    public static final String SERVICE_NAME = "product.ProductGrpc";

    // Static method descriptors that strictly reflect the proto.
    private static volatile io.grpc.MethodDescriptor<ProductSaveRequest, ProductSaveResponse> getSaveMethod;

    @io.grpc.stub.annotations.RpcMethod(fullMethodName = SERVICE_NAME + '/' + "save", requestType = ProductSaveRequest.class, responseType = ProductSaveResponse.class, methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
    public static io.grpc.MethodDescriptor<ProductSaveRequest, ProductSaveResponse> getSaveMethod() {
        io.grpc.MethodDescriptor<ProductSaveRequest, ProductSaveResponse> getSaveMethod;
        if ((getSaveMethod = ProductGrpcGrpc.getSaveMethod) == null) {
            synchronized (ProductGrpcGrpc.class) {
                if ((getSaveMethod = ProductGrpcGrpc.getSaveMethod) == null) {
                    ProductGrpcGrpc.getSaveMethod = getSaveMethod = io.grpc.MethodDescriptor.<ProductSaveRequest, ProductSaveResponse>newBuilder().setType(io.grpc.MethodDescriptor.MethodType.UNARY).setFullMethodName(generateFullMethodName(SERVICE_NAME, "save")).setSampledToLocalTracing(true).setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(ProductSaveRequest.getDefaultInstance())).setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(ProductSaveResponse.getDefaultInstance())).setSchemaDescriptor(new ProductGrpcMethodDescriptorSupplier("save")).build();
                }
            }
        }
        return getSaveMethod;
    }

    /**
     * Creates a new async stub that supports all call types for the service
     */
    public static ProductGrpcStub newStub(io.grpc.Channel channel) {
        io.grpc.stub.AbstractStub.StubFactory<ProductGrpcStub> factory = new io.grpc.stub.AbstractStub.StubFactory<ProductGrpcStub>() {

            @Override
            public ProductGrpcStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
                return new ProductGrpcStub(channel, callOptions);
            }
        };
        return ProductGrpcStub.newStub(factory, channel);
    }

    /**
     * Creates a new blocking-style stub that supports unary and streaming output calls on the service
     */
    public static ProductGrpcBlockingStub newBlockingStub(io.grpc.Channel channel) {
        io.grpc.stub.AbstractStub.StubFactory<ProductGrpcBlockingStub> factory = new io.grpc.stub.AbstractStub.StubFactory<ProductGrpcBlockingStub>() {

            @Override
            public ProductGrpcBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
                return new ProductGrpcBlockingStub(channel, callOptions);
            }
        };
        return ProductGrpcBlockingStub.newStub(factory, channel);
    }

    /**
     * Creates a new ListenableFuture-style stub that supports unary calls on the service
     */
    public static ProductGrpcFutureStub newFutureStub(io.grpc.Channel channel) {
        io.grpc.stub.AbstractStub.StubFactory<ProductGrpcFutureStub> factory = new io.grpc.stub.AbstractStub.StubFactory<ProductGrpcFutureStub>() {

            @Override
            public ProductGrpcFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
                return new ProductGrpcFutureStub(channel, callOptions);
            }
        };
        return ProductGrpcFutureStub.newStub(factory, channel);
    }

    /**
     */
    public static abstract class ProductGrpcImplBase implements io.grpc.BindableService {

        /**
         */
        public void save(ProductSaveRequest request, io.grpc.stub.StreamObserver<ProductSaveResponse> responseObserver) {
            io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSaveMethod(), responseObserver);
        }

        @Override
        public io.grpc.ServerServiceDefinition bindService() {
            return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor()).addMethod(getSaveMethod(), io.grpc.stub.ServerCalls.asyncUnaryCall(new MethodHandlers<ProductSaveRequest, ProductSaveResponse>(this, METHODID_SAVE))).build();
        }
    }

    /**
     */
    public static class ProductGrpcStub extends io.grpc.stub.AbstractAsyncStub<ProductGrpcStub> {

        private ProductGrpcStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override
        protected ProductGrpcStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new ProductGrpcStub(channel, callOptions);
        }

        /**
         */
        public void save(ProductSaveRequest request, io.grpc.stub.StreamObserver<ProductSaveResponse> responseObserver) {
            io.grpc.stub.ClientCalls.asyncUnaryCall(getChannel().newCall(getSaveMethod(), getCallOptions()), request, responseObserver);
        }
    }

    /**
     */
    public static class ProductGrpcBlockingStub extends io.grpc.stub.AbstractBlockingStub<ProductGrpcBlockingStub> {

        private ProductGrpcBlockingStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override
        protected ProductGrpcBlockingStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new ProductGrpcBlockingStub(channel, callOptions);
        }

        /**
         */
        public ProductSaveResponse save(ProductSaveRequest request) {
            return io.grpc.stub.ClientCalls.blockingUnaryCall(getChannel(), getSaveMethod(), getCallOptions(), request);
        }
    }

    /**
     */
    public static class ProductGrpcFutureStub extends io.grpc.stub.AbstractFutureStub<ProductGrpcFutureStub> {

        private ProductGrpcFutureStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override
        protected ProductGrpcFutureStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new ProductGrpcFutureStub(channel, callOptions);
        }

        /**
         */
        public com.google.common.util.concurrent.ListenableFuture<ProductSaveResponse> save(ProductSaveRequest request) {
            return io.grpc.stub.ClientCalls.futureUnaryCall(getChannel().newCall(getSaveMethod(), getCallOptions()), request);
        }
    }

    private static final int METHODID_SAVE = 0;

    private static final class MethodHandlers<Req, Resp> implements io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>, io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>, io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>, io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {

        private final ProductGrpcImplBase serviceImpl;

        private final int methodId;

        MethodHandlers(ProductGrpcImplBase serviceImpl, int methodId) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
        }

        @Override
        @SuppressWarnings("unchecked")
        public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch(methodId) {
                case METHODID_SAVE:
                    serviceImpl.save((ProductSaveRequest) request, (io.grpc.stub.StreamObserver<ProductSaveResponse>) responseObserver);
                    break;
                default:
                    throw new AssertionError();
            }
        }

        @Override
        @SuppressWarnings("unchecked")
        public io.grpc.stub.StreamObserver<Req> invoke(io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch(methodId) {
                default:
                    throw new AssertionError();
            }
        }
    }

    private static abstract class ProductGrpcBaseDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {

        ProductGrpcBaseDescriptorSupplier() {
        }

        @Override
        public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
            return CustomerGrpcProto.getDescriptor();
        }

        @Override
        public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
            return getFileDescriptor().findServiceByName("ProductGrpc");
        }
    }

    private static final class ProductGrpcFileDescriptorSupplier extends ProductGrpcBaseDescriptorSupplier {

        ProductGrpcFileDescriptorSupplier() {
        }
    }

    private static final class ProductGrpcMethodDescriptorSupplier extends ProductGrpcBaseDescriptorSupplier implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {

        private final String methodName;

        ProductGrpcMethodDescriptorSupplier(String methodName) {
            this.methodName = methodName;
        }

        @Override
        public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
            return getServiceDescriptor().findMethodByName(methodName);
        }
    }

    private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

    public static io.grpc.ServiceDescriptor getServiceDescriptor() {
        io.grpc.ServiceDescriptor result = serviceDescriptor;
        if (result == null) {
            synchronized (ProductGrpcGrpc.class) {
                result = serviceDescriptor;
                if (result == null) {
                    serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME).setSchemaDescriptor(new ProductGrpcFileDescriptorSupplier()).addMethod(getSaveMethod()).build();
                }
            }
        }
        return result;
    }
}

