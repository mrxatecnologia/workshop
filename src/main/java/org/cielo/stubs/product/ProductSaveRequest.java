// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: product.proto
package org.cielo.stubs.product;

/**
 * Protobuf type {@code product.ProductSaveRequest}
 */
public final class ProductSaveRequest extends com.google.protobuf.GeneratedMessageV3 implements // @@protoc_insertion_point(message_implements:product.ProductSaveRequest)
        ProductSaveRequestOrBuilder {

    private static final long serialVersionUID = 0L;

    // Use ProductSaveRequest.newBuilder() to construct.
    private ProductSaveRequest(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
        super(builder);
    }

    private ProductSaveRequest() {
        description_ = "";
    }

    @Override
    @SuppressWarnings({ "unused" })
    protected Object newInstance(UnusedPrivateParameter unused) {
        return new ProductSaveRequest();
    }

    @Override
    public final com.google.protobuf.UnknownFieldSet getUnknownFields() {
        return this.unknownFields;
    }

    public static final com.google.protobuf.Descriptors.Descriptor getDescriptor() {
        return CustomerGrpcProto.internal_static_product_ProductSaveRequest_descriptor;
    }

    @Override
    protected FieldAccessorTable internalGetFieldAccessorTable() {
        return CustomerGrpcProto.internal_static_product_ProductSaveRequest_fieldAccessorTable.ensureFieldAccessorsInitialized(ProductSaveRequest.class, ProductSaveRequest.Builder.class);
    }

    public static final int DESCRIPTION_FIELD_NUMBER = 1;

    private volatile Object description_;

    /**
     * <code>string description = 1;</code>
     * @return The description.
     */
    @Override
    public String getDescription() {
        Object ref = description_;
        if (ref instanceof String) {
            return (String) ref;
        } else {
            com.google.protobuf.ByteString bs = (com.google.protobuf.ByteString) ref;
            String s = bs.toStringUtf8();
            description_ = s;
            return s;
        }
    }

    /**
     * <code>string description = 1;</code>
     * @return The bytes for description.
     */
    @Override
    public com.google.protobuf.ByteString getDescriptionBytes() {
        Object ref = description_;
        if (ref instanceof String) {
            com.google.protobuf.ByteString b = com.google.protobuf.ByteString.copyFromUtf8((String) ref);
            description_ = b;
            return b;
        } else {
            return (com.google.protobuf.ByteString) ref;
        }
    }

    public static final int PRICE_FIELD_NUMBER = 2;

    private long price_;

    /**
     * <code>int64 price = 2;</code>
     * @return The price.
     */
    @Override
    public long getPrice() {
        return price_;
    }

    private byte memoizedIsInitialized = -1;

    @Override
    public final boolean isInitialized() {
        byte isInitialized = memoizedIsInitialized;
        if (isInitialized == 1)
            return true;
        if (isInitialized == 0)
            return false;
        memoizedIsInitialized = 1;
        return true;
    }

    @Override
    public void writeTo(com.google.protobuf.CodedOutputStream output) throws java.io.IOException {
        if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(description_)) {
            com.google.protobuf.GeneratedMessageV3.writeString(output, 1, description_);
        }
        if (price_ != 0L) {
            output.writeInt64(2, price_);
        }
        getUnknownFields().writeTo(output);
    }

    @Override
    public int getSerializedSize() {
        int size = memoizedSize;
        if (size != -1)
            return size;
        size = 0;
        if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(description_)) {
            size += com.google.protobuf.GeneratedMessageV3.computeStringSize(1, description_);
        }
        if (price_ != 0L) {
            size += com.google.protobuf.CodedOutputStream.computeInt64Size(2, price_);
        }
        size += getUnknownFields().getSerializedSize();
        memoizedSize = size;
        return size;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ProductSaveRequest)) {
            return super.equals(obj);
        }
        ProductSaveRequest other = (ProductSaveRequest) obj;
        if (!getDescription().equals(other.getDescription()))
            return false;
        if (getPrice() != other.getPrice())
            return false;
        if (!getUnknownFields().equals(other.getUnknownFields()))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        if (memoizedHashCode != 0) {
            return memoizedHashCode;
        }
        int hash = 41;
        hash = (19 * hash) + getDescriptor().hashCode();
        hash = (37 * hash) + DESCRIPTION_FIELD_NUMBER;
        hash = (53 * hash) + getDescription().hashCode();
        hash = (37 * hash) + PRICE_FIELD_NUMBER;
        hash = (53 * hash) + com.google.protobuf.Internal.hashLong(getPrice());
        hash = (29 * hash) + getUnknownFields().hashCode();
        memoizedHashCode = hash;
        return hash;
    }

    public static ProductSaveRequest parseFrom(java.nio.ByteBuffer data) throws com.google.protobuf.InvalidProtocolBufferException {
        return PARSER.parseFrom(data);
    }

    public static ProductSaveRequest parseFrom(java.nio.ByteBuffer data, com.google.protobuf.ExtensionRegistryLite extensionRegistry) throws com.google.protobuf.InvalidProtocolBufferException {
        return PARSER.parseFrom(data, extensionRegistry);
    }

    public static ProductSaveRequest parseFrom(com.google.protobuf.ByteString data) throws com.google.protobuf.InvalidProtocolBufferException {
        return PARSER.parseFrom(data);
    }

    public static ProductSaveRequest parseFrom(com.google.protobuf.ByteString data, com.google.protobuf.ExtensionRegistryLite extensionRegistry) throws com.google.protobuf.InvalidProtocolBufferException {
        return PARSER.parseFrom(data, extensionRegistry);
    }

    public static ProductSaveRequest parseFrom(byte[] data) throws com.google.protobuf.InvalidProtocolBufferException {
        return PARSER.parseFrom(data);
    }

    public static ProductSaveRequest parseFrom(byte[] data, com.google.protobuf.ExtensionRegistryLite extensionRegistry) throws com.google.protobuf.InvalidProtocolBufferException {
        return PARSER.parseFrom(data, extensionRegistry);
    }

    public static ProductSaveRequest parseFrom(java.io.InputStream input) throws java.io.IOException {
        return com.google.protobuf.GeneratedMessageV3.parseWithIOException(PARSER, input);
    }

    public static ProductSaveRequest parseFrom(java.io.InputStream input, com.google.protobuf.ExtensionRegistryLite extensionRegistry) throws java.io.IOException {
        return com.google.protobuf.GeneratedMessageV3.parseWithIOException(PARSER, input, extensionRegistry);
    }

    public static ProductSaveRequest parseDelimitedFrom(java.io.InputStream input) throws java.io.IOException {
        return com.google.protobuf.GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input);
    }

    public static ProductSaveRequest parseDelimitedFrom(java.io.InputStream input, com.google.protobuf.ExtensionRegistryLite extensionRegistry) throws java.io.IOException {
        return com.google.protobuf.GeneratedMessageV3.parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }

    public static ProductSaveRequest parseFrom(com.google.protobuf.CodedInputStream input) throws java.io.IOException {
        return com.google.protobuf.GeneratedMessageV3.parseWithIOException(PARSER, input);
    }

    public static ProductSaveRequest parseFrom(com.google.protobuf.CodedInputStream input, com.google.protobuf.ExtensionRegistryLite extensionRegistry) throws java.io.IOException {
        return com.google.protobuf.GeneratedMessageV3.parseWithIOException(PARSER, input, extensionRegistry);
    }

    @Override
    public Builder newBuilderForType() {
        return newBuilder();
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.toBuilder();
    }

    public static Builder newBuilder(ProductSaveRequest prototype) {
        return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }

    @Override
    public Builder toBuilder() {
        return this == DEFAULT_INSTANCE ? new Builder() : new Builder().mergeFrom(this);
    }

    @Override
    protected Builder newBuilderForType(BuilderParent parent) {
        Builder builder = new Builder(parent);
        return builder;
    }

    /**
     * Protobuf type {@code product.ProductSaveRequest}
     */
    public static final class Builder extends com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements // @@protoc_insertion_point(builder_implements:product.ProductSaveRequest)
            ProductSaveRequestOrBuilder {

        public static final com.google.protobuf.Descriptors.Descriptor getDescriptor() {
            return CustomerGrpcProto.internal_static_product_ProductSaveRequest_descriptor;
        }

        @Override
        protected FieldAccessorTable internalGetFieldAccessorTable() {
            return CustomerGrpcProto.internal_static_product_ProductSaveRequest_fieldAccessorTable.ensureFieldAccessorsInitialized(ProductSaveRequest.class, ProductSaveRequest.Builder.class);
        }

        // Construct using org.cielo.stubs.product.ProductSaveRequest.newBuilder()
        private Builder() {
        }

        private Builder(BuilderParent parent) {
            super(parent);
        }

        @Override
        public Builder clear() {
            super.clear();
            description_ = "";
            price_ = 0L;
            return this;
        }

        @Override
        public com.google.protobuf.Descriptors.Descriptor getDescriptorForType() {
            return CustomerGrpcProto.internal_static_product_ProductSaveRequest_descriptor;
        }

        @Override
        public ProductSaveRequest getDefaultInstanceForType() {
            return ProductSaveRequest.getDefaultInstance();
        }

        @Override
        public ProductSaveRequest build() {
            ProductSaveRequest result = buildPartial();
            if (!result.isInitialized()) {
                throw newUninitializedMessageException(result);
            }
            return result;
        }

        @Override
        public ProductSaveRequest buildPartial() {
            ProductSaveRequest result = new ProductSaveRequest(this);
            result.description_ = description_;
            result.price_ = price_;
            onBuilt();
            return result;
        }

        @Override
        public Builder clone() {
            return super.clone();
        }

        @Override
        public Builder setField(com.google.protobuf.Descriptors.FieldDescriptor field, Object value) {
            return super.setField(field, value);
        }

        @Override
        public Builder clearField(com.google.protobuf.Descriptors.FieldDescriptor field) {
            return super.clearField(field);
        }

        @Override
        public Builder clearOneof(com.google.protobuf.Descriptors.OneofDescriptor oneof) {
            return super.clearOneof(oneof);
        }

        @Override
        public Builder setRepeatedField(com.google.protobuf.Descriptors.FieldDescriptor field, int index, Object value) {
            return super.setRepeatedField(field, index, value);
        }

        @Override
        public Builder addRepeatedField(com.google.protobuf.Descriptors.FieldDescriptor field, Object value) {
            return super.addRepeatedField(field, value);
        }

        @Override
        public Builder mergeFrom(com.google.protobuf.Message other) {
            if (other instanceof ProductSaveRequest) {
                return mergeFrom((ProductSaveRequest) other);
            } else {
                super.mergeFrom(other);
                return this;
            }
        }

        public Builder mergeFrom(ProductSaveRequest other) {
            if (other == ProductSaveRequest.getDefaultInstance())
                return this;
            if (!other.getDescription().isEmpty()) {
                description_ = other.description_;
                onChanged();
            }
            if (other.getPrice() != 0L) {
                setPrice(other.getPrice());
            }
            this.mergeUnknownFields(other.getUnknownFields());
            onChanged();
            return this;
        }

        @Override
        public final boolean isInitialized() {
            return true;
        }

        @Override
        public Builder mergeFrom(com.google.protobuf.CodedInputStream input, com.google.protobuf.ExtensionRegistryLite extensionRegistry) throws java.io.IOException {
            if (extensionRegistry == null) {
                throw new NullPointerException();
            }
            try {
                boolean done = false;
                while (!done) {
                    int tag = input.readTag();
                    switch(tag) {
                        case 0:
                            done = true;
                            break;
                        case 10:
                            {
                                description_ = input.readStringRequireUtf8();
                                break;
                            }
                        // case 10
                        case 16:
                            {
                                price_ = input.readInt64();
                                break;
                            }
                        // case 16
                        default:
                            {
                                if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                                    // was an endgroup tag
                                    done = true;
                                }
                                break;
                            }
                    }
                    // switch (tag)
                }
                // while (!done)
            } catch (com.google.protobuf.InvalidProtocolBufferException e) {
                throw e.unwrapIOException();
            } finally {
                onChanged();
            }
            // finally
            return this;
        }

        private Object description_ = "";

        /**
         * <code>string description = 1;</code>
         * @return The description.
         */
        public String getDescription() {
            Object ref = description_;
            if (!(ref instanceof String)) {
                com.google.protobuf.ByteString bs = (com.google.protobuf.ByteString) ref;
                String s = bs.toStringUtf8();
                description_ = s;
                return s;
            } else {
                return (String) ref;
            }
        }

        /**
         * <code>string description = 1;</code>
         * @return The bytes for description.
         */
        public com.google.protobuf.ByteString getDescriptionBytes() {
            Object ref = description_;
            if (ref instanceof String) {
                com.google.protobuf.ByteString b = com.google.protobuf.ByteString.copyFromUtf8((String) ref);
                description_ = b;
                return b;
            } else {
                return (com.google.protobuf.ByteString) ref;
            }
        }

        /**
         * <code>string description = 1;</code>
         * @param value The description to set.
         * @return This builder for chaining.
         */
        public Builder setDescription(String value) {
            if (value == null) {
                throw new NullPointerException();
            }
            description_ = value;
            onChanged();
            return this;
        }

        /**
         * <code>string description = 1;</code>
         * @return This builder for chaining.
         */
        public Builder clearDescription() {
            description_ = getDefaultInstance().getDescription();
            onChanged();
            return this;
        }

        /**
         * <code>string description = 1;</code>
         * @param value The bytes for description to set.
         * @return This builder for chaining.
         */
        public Builder setDescriptionBytes(com.google.protobuf.ByteString value) {
            if (value == null) {
                throw new NullPointerException();
            }
            checkByteStringIsUtf8(value);
            description_ = value;
            onChanged();
            return this;
        }

        private long price_;

        /**
         * <code>int64 price = 2;</code>
         * @return The price.
         */
        @Override
        public long getPrice() {
            return price_;
        }

        /**
         * <code>int64 price = 2;</code>
         * @param value The price to set.
         * @return This builder for chaining.
         */
        public Builder setPrice(long value) {
            price_ = value;
            onChanged();
            return this;
        }

        /**
         * <code>int64 price = 2;</code>
         * @return This builder for chaining.
         */
        public Builder clearPrice() {
            price_ = 0L;
            onChanged();
            return this;
        }

        @Override
        public final Builder setUnknownFields(final com.google.protobuf.UnknownFieldSet unknownFields) {
            return super.setUnknownFields(unknownFields);
        }

        @Override
        public final Builder mergeUnknownFields(final com.google.protobuf.UnknownFieldSet unknownFields) {
            return super.mergeUnknownFields(unknownFields);
        }
        // @@protoc_insertion_point(builder_scope:product.ProductSaveRequest)
    }

    // @@protoc_insertion_point(class_scope:product.ProductSaveRequest)
    private static final ProductSaveRequest DEFAULT_INSTANCE;

    static {
        DEFAULT_INSTANCE = new ProductSaveRequest();
    }

    public static ProductSaveRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<ProductSaveRequest> PARSER = new com.google.protobuf.AbstractParser<ProductSaveRequest>() {

        @Override
        public ProductSaveRequest parsePartialFrom(com.google.protobuf.CodedInputStream input, com.google.protobuf.ExtensionRegistryLite extensionRegistry) throws com.google.protobuf.InvalidProtocolBufferException {
            Builder builder = newBuilder();
            try {
                builder.mergeFrom(input, extensionRegistry);
            } catch (com.google.protobuf.InvalidProtocolBufferException e) {
                throw e.setUnfinishedMessage(builder.buildPartial());
            } catch (com.google.protobuf.UninitializedMessageException e) {
                throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
            } catch (java.io.IOException e) {
                throw new com.google.protobuf.InvalidProtocolBufferException(e).setUnfinishedMessage(builder.buildPartial());
            }
            return builder.buildPartial();
        }
    };

    public static com.google.protobuf.Parser<ProductSaveRequest> parser() {
        return PARSER;
    }

    @Override
    public com.google.protobuf.Parser<ProductSaveRequest> getParserForType() {
        return PARSER;
    }

    @Override
    public ProductSaveRequest getDefaultInstanceForType() {
        return DEFAULT_INSTANCE;
    }
}

