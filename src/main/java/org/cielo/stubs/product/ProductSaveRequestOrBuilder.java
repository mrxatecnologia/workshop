// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: product.proto
package org.cielo.stubs.product;

public interface ProductSaveRequestOrBuilder extends // @@protoc_insertion_point(interface_extends:product.ProductSaveRequest)
com.google.protobuf.MessageOrBuilder {

    /**
     * <code>string description = 1;</code>
     * @return The description.
     */
    String getDescription();

    /**
     * <code>string description = 1;</code>
     * @return The bytes for description.
     */
    com.google.protobuf.ByteString getDescriptionBytes();

    /**
     * <code>int64 price = 2;</code>
     * @return The price.
     */
    long getPrice();
}

