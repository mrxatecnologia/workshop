package org.cielo.translator;

import io.smallrye.mutiny.Uni;
import org.cielo.domain.Customer;
import org.cielo.stubs.customer.CustomerSaveRequest;
import org.cielo.stubs.customer.CustomerSaveResponse;

import java.util.logging.Logger;


public class CustomerTranslator {

    private static final Logger LOGGER = Logger.getLogger("CustomerTranslator.class");

    public static Customer translateRequestToEntity(CustomerSaveRequest request) {
        LOGGER.info("Start Parsing request to entity");
        final Customer customer = new Customer();
        customer.setAge(request.getAge());
        customer.setName(request.getName());
        customer.setStreetAddress(request.getStreetAddress());
        LOGGER.info("Finish Parsing request to entity");
        return customer;
    }

    public static Uni<CustomerSaveResponse> translateEntityToResponse(Customer saved) {
        return Uni.createFrom().item(() ->
                CustomerSaveResponse.newBuilder().setResponseMessage("Customer saved with sucess").build());
    }
}
